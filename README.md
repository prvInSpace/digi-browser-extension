# Digi Browser Extension

Adds https://digi.prv.cymru as a little window on your toolbar to make searching for words, conjugations, and stuff like that easier.

## Maintainer

Preben Vangberg, prv21fgt@bangor.ac.uk
